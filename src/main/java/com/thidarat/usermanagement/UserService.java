/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thidarat.usermanagement;

import java.util.ArrayList;

/**
 *
 * @author acer
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();

    // Mockup จำลองข้อมูล
    //static box
    static {
        userList.add(new User("admin", "password"));
        userList.add(new User("user1", "password"));
    }
    // creat Methond

    public static boolean addUser(User user) { // เพิ่ม user
        userList.add(user); //เพิ่มเข้าList
        return true; // เพิ่มได้
    }

    //Overload
    public static boolean addUser(String userName, String password) { // เพิ่ม user
        userList.add(new User(userName, password)); //เพิ่มเข้าList
        return true; // เพิ่มได้
    }

    public static boolean updateUser(int index, User user) { // อัพเดตข้อมูล
        userList.set(index, user); //อัพเดตList
        return true;//อัพเดตได้
    }

    // Read 1 User
    public static User getUser(int index) { // ดึง user จากตำ่่หน่ง
       
        if(index<userList.size()-1){
            return null;
        }
        
        return userList.get(index); //ดึง user จากตำ่่หน่งใน List
    }

    // Read All User
    public static ArrayList<User> getUsers() { // ดึง user จากList
        return userList; //ดึง user จาก List
    }

    // Search username
    public static ArrayList<User> searchUserName(String searchText) { //ค้นหาชื่อ
        ArrayList<User> list = new ArrayList<>(); //เพื่อมาเก็บรายชื่อ List ว่าง

        for (User user : userList) { // วนลูป
            if (user.getUserName().startsWith(searchText)) { // ค้นหาชื่อ
                list.add(user); //เพิ่มชื่อเข้า List ที่เตรียมไว้
            }
        }

        return list; // โชว์ค่าที่เก็บใน List
    }
    
    public static boolean delUser(int index){ //ลบข้อมูล เเบบใน index
        userList.remove(index);
        return true; //ลบได้
    }
       public static boolean delUser(User user){ //ลบข้อมูล เเบบ Object
        userList.remove(user);
        return true; //ลบได้
    }
       
    // Loin
     public static User login(String userName,String password ){
         for (User user : userList) { ; //วนลูปหา User
             if(user.getUserName().equals(userName)&&user.getPassword().equals(password)){ //เช็คชื่อเเละรหัสผ่านว่าตรงกันไหม
                 return user; // ส่งกลับไปเป็น ชื่อ
             }
         }
         
         return null; //หาไม่เจอ ส่งกลับไปเป็น null
     }
     
     // save
     public static void save(){
         
     }
    
     // load
     public static void load(){
         
     }
    

}
