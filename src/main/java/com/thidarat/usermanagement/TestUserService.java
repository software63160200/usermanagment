/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.thidarat.usermanagement;

/**
 *
 * @author acer
 */
public class TestUserService {

    public static void main(String[] args) {
        
        UserService.addUser("user2", "password"); // เพิ่ม User
        System.out.println(UserService.getUsers());
        
        UserService.addUser(new User("user3", "password")); // เพิ่ม User
        System.out.println(UserService.getUsers());
        
        User user =UserService.getUser(3); // หา User
        System.out.println(user);
        
        user.setPassword("1234"); // เปลนี่นรหัส
        UserService.updateUser(3, user);//  อัพเดต User 
        System.out.println(user);
         
        UserService.delUser(user);// ลบ User
        System.out.println(UserService.getUsers());
        
        System.out.println(UserService.login("admin", "password")); //Login
    }
}
